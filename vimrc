filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" " when indenting with '>', use 4 spaces width
set shiftwidth=4
" " On pressing tab, insert 4 spaces
set noexpandtab
set autoindent
set hlsearch
set incsearch

" associate composer.lock with json filetype
au BufRead,BufNewFile composer.lock setfiletype js

:color desert
hi Comment ctermfg=gray
